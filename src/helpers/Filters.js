import Vue from 'vue'

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString().toLowerCase().replace(/(?:^|\s)\S/g, l => l.toUpperCase())
  return value
})

Vue.filter('lowercase', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.toLowerCase()
})

Vue.filter('uppercase', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.toUpperCase()
})

Vue.filter('validateUndefined', function (value) {
  if (value === undefined || value === null) {
    return ''
  } else {
    return value
  }
})
