import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import './helpers/Filters'

Vue.config.productionTip = false

if (process.env.NODE_ENV === 'development') {
  Vue.config.devtools = true
}

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
}).$mount('#app')

axios.defaults.baseURL = 'http://localhost:8070'

/* Url intersect */
const pathActual = window.location.origin
if (pathActual.includes(process.env.VUE_APP_QA) || pathActual.includes(process.env.VUE_APP_PRO)) {
  axios.interceptors.response.use(response => {
    return Promise.reject(response)
  }, error => {
    return Promise.reject(error)
  })
}
