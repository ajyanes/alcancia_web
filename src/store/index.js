import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tiposDocumentoArray: [
      { id: 'RC', value: 'Registro civil de nacimiento' },
      { id: 'TI', value: 'Tarjeta de identidad' },
      { id: 'CC', value: 'Cédula de ciudadanía' },
      { id: 'AS', value: 'Adulto sin documento de identificación' },
      { id: 'CD', value: 'Carné diplomático' },
      { id: 'CE', value: 'Cédula de extranjería' },
      { id: 'CN', value: 'Certificado de nacido vivo - DANE' },
      { id: 'MS', value: 'Menor sin documento de identificación' },
      { id: 'NI', value: 'NIT' },
      { id: 'ND', value: 'No Definido' },
      { id: 'NU', value: 'NUIP' },
      { id: 'PS', value: 'Pasaporte' },
      { id: 'PA', value: 'Pasaporte expedido por el país de origen, sólo para extranjeros' },
      { id: 'PE', value: 'Permiso especial de permanencia' },
      { id: 'SC', value: 'Salvoconducto de permanencia' }
    ],
    departmentsList: [],
    listMunicipaly: [],
    credentials: null,
    currentUser: null
  },
  mutations: {
    setDepartmentsList (state, val) {
      state.departmentsList = val
    },
    setListMunicipaly (state, val) {
      state.listMunicipaly = val
    },
    setCredentials (state, val) {
      state.credentials = val
    },
    setCurrentUser (state, val) {
      state.currentUser = val
    }
  },
  actions: {
    getDepartmentList ({ commit }) {
      const endpoint = process.env.VUE_APP_API_GATEWAY + '/commons/states'
      axios.get(endpoint).then(res => {
        commit('setDepartmentsList', res.data)
      })
    },
    getMunicipalityList ({ commit }, code) {
      const endpoint = process.env.VUE_APP_API_GATEWAY + '/commons/states/' + code + '/towns'
      axios.get(endpoint).then(res => {
        commit('setListMunicipaly', res.data)
      }).catch(() => {
      })
    }
  },
  modules: {
  }
})
