import Vue from 'vue'
import VueRouter from 'vue-router'
import Alcancia from '@/views/Alcancia'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/piggy-bank'
  },
  {
    path: '/piggy-bank',
    name: 'Alcancia',
    component: Alcancia
  }
]

const router = new VueRouter({
  routes
})

export default router
